/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "proyectos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proyectos.findAll", query = "SELECT p FROM Proyectos p")
    , @NamedQuery(name = "Proyectos.findByIdproyecto", query = "SELECT p FROM Proyectos p WHERE p.proyectosPK.idproyecto = :idproyecto")
    , @NamedQuery(name = "Proyectos.findByIdequipo", query = "SELECT p FROM Proyectos p WHERE p.proyectosPK.idequipo = :idequipo")
    , @NamedQuery(name = "Proyectos.findByNombreproyecto", query = "SELECT p FROM Proyectos p WHERE p.nombreproyecto = :nombreproyecto")
    , @NamedQuery(name = "Proyectos.findByDescripcion", query = "SELECT p FROM Proyectos p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Proyectos.findByEstado", query = "SELECT p FROM Proyectos p WHERE p.estado = :estado")})
public class Proyectos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProyectosPK proyectosPK;
    @Size(max = 50)
    @Column(name = "nombreproyecto")
    private String nombreproyecto;
    @Size(max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 15)
    @Column(name = "estado")
    private String estado;
    @JoinColumn(name = "idequipo", referencedColumnName = "idequipo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Equipos equipos;

    public Proyectos() {
    }

    public Proyectos(ProyectosPK proyectosPK) {
        this.proyectosPK = proyectosPK;
    }

    public Proyectos(int idproyecto, int idequipo) {
        this.proyectosPK = new ProyectosPK(idproyecto, idequipo);
    }

    public ProyectosPK getProyectosPK() {
        return proyectosPK;
    }

    public void setProyectosPK(ProyectosPK proyectosPK) {
        this.proyectosPK = proyectosPK;
    }

    public String getNombreproyecto() {
        return nombreproyecto;
    }

    public void setNombreproyecto(String nombreproyecto) {
        this.nombreproyecto = nombreproyecto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Equipos getEquipos() {
        return equipos;
    }

    public void setEquipos(Equipos equipos) {
        this.equipos = equipos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyectosPK != null ? proyectosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyectos)) {
            return false;
        }
        Proyectos other = (Proyectos) object;
        if ((this.proyectosPK == null && other.proyectosPK != null) || (this.proyectosPK != null && !this.proyectosPK.equals(other.proyectosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Proyectos[ proyectosPK=" + proyectosPK + " ]";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author elias
 */
@Embeddable
public class ProyectosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "idproyecto")
    private int idproyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idequipo")
    private int idequipo;

    public ProyectosPK() {
    }

    public ProyectosPK(int idproyecto, int idequipo) {
        this.idproyecto = idproyecto;
        this.idequipo = idequipo;
    }

    public int getIdproyecto() {
        return idproyecto;
    }

    public void setIdproyecto(int idproyecto) {
        this.idproyecto = idproyecto;
    }

    public int getIdequipo() {
        return idequipo;
    }

    public void setIdequipo(int idequipo) {
        this.idequipo = idequipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idproyecto;
        hash += (int) idequipo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyectosPK)) {
            return false;
        }
        ProyectosPK other = (ProyectosPK) object;
        if (this.idproyecto != other.idproyecto) {
            return false;
        }
        if (this.idequipo != other.idequipo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ProyectosPK[ idproyecto=" + idproyecto + ", idequipo=" + idequipo + " ]";
    }
    
}

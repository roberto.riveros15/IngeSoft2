/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author elias
 */
@Embeddable
public class MiembrosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "idusuario")
    private int idusuario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idequipo")
    private int idequipo;

    public MiembrosPK() {
    }

    public MiembrosPK(int idusuario, int idequipo) {
        this.idusuario = idusuario;
        this.idequipo = idequipo;
    }

    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public int getIdequipo() {
        return idequipo;
    }

    public void setIdequipo(int idequipo) {
        this.idequipo = idequipo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idusuario;
        hash += (int) idequipo;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MiembrosPK)) {
            return false;
        }
        MiembrosPK other = (MiembrosPK) object;
        if (this.idusuario != other.idusuario) {
            return false;
        }
        if (this.idequipo != other.idequipo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.MiembrosPK[ idusuario=" + idusuario + ", idequipo=" + idequipo + " ]";
    }
    
}

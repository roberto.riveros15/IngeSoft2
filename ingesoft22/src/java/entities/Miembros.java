/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author elias
 */
@Entity
@Table(name = "miembros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Miembros.findAll", query = "SELECT m FROM Miembros m")
    , @NamedQuery(name = "Miembros.findByIdusuario", query = "SELECT m FROM Miembros m WHERE m.miembrosPK.idusuario = :idusuario")
    , @NamedQuery(name = "Miembros.findByIdequipo", query = "SELECT m FROM Miembros m WHERE m.miembrosPK.idequipo = :idequipo")})
public class Miembros implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MiembrosPK miembrosPK;
    @JoinColumn(name = "idequipo", referencedColumnName = "idequipo", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Equipos equipos;
    @JoinColumn(name = "idrol", referencedColumnName = "idrol")
    @ManyToOne(optional = false)
    private Roles idrol;
    @JoinColumn(name = "idtarea", referencedColumnName = "idtarea")
    @ManyToOne(optional = false)
    private Tareas idtarea;
    @JoinColumn(name = "idusuario", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuarios usuarios;

    public Miembros() {
    }

    public Miembros(MiembrosPK miembrosPK) {
        this.miembrosPK = miembrosPK;
    }

    public Miembros(int idusuario, int idequipo) {
        this.miembrosPK = new MiembrosPK(idusuario, idequipo);
    }

    public MiembrosPK getMiembrosPK() {
        return miembrosPK;
    }

    public void setMiembrosPK(MiembrosPK miembrosPK) {
        this.miembrosPK = miembrosPK;
    }

    public Equipos getEquipos() {
        return equipos;
    }

    public void setEquipos(Equipos equipos) {
        this.equipos = equipos;
    }

    public Roles getIdrol() {
        return idrol;
    }

    public void setIdrol(Roles idrol) {
        this.idrol = idrol;
    }

    public Tareas getIdtarea() {
        return idtarea;
    }

    public void setIdtarea(Tareas idtarea) {
        this.idtarea = idtarea;
    }

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (miembrosPK != null ? miembrosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Miembros)) {
            return false;
        }
        Miembros other = (Miembros) object;
        if ((this.miembrosPK == null && other.miembrosPK != null) || (this.miembrosPK != null && !this.miembrosPK.equals(other.miembrosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Miembros[ miembrosPK=" + miembrosPK + " ]";
    }
    
}

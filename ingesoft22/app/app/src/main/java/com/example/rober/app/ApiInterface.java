package com.example.rober.app;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiInterface {
    @GET("/Entities.usuarios/Count")
    Call<List<ApiInterface>> listRepos(@Path("user") String user);

    @POST("/Entities.usuarios/Post")
    Call<List<ApiInterface>> listRespon(@Path("user") String user);
}

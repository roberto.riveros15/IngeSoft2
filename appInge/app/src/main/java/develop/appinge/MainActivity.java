package develop.appinge;

import android.net.Uri;
import android.os.StrictMode;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.prefs.Preferences;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }


    public void login(View v) throws IOException, JSONException {
       EditText editTextUsername =  findViewById(R.id.editText);
       String username = editTextUsername.getText().toString();
       EditText editTextPass = findViewById(R.id.editText2);
       String password = editTextPass.getText().toString();
       String resp = register(username,password);
       Toast.makeText(this, resp, Toast.LENGTH_LONG).show();
    }


    public String register (String username, String password) throws IOException, JSONException {
        JSONObject credenciales = new JSONObject();
        credenciales.put("nombre", username);
        credenciales.put("password", password);
        URL url = new URL("http://10.13.16.53:10881/ingesoft22/webresources/entities.usuarios/login");
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
        }catch (Exception e){
            e.printStackTrace();
        }
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setDoOutput(false);
        connection.setDoInput(true);
        connection.setConnectTimeout(7000);
        connection.setReadTimeout(7000);
        try {
            OutputStream wr = connection.getOutputStream();
            wr.write(credenciales.toString().getBytes());
            wr.flush();
            wr.close();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Y AQUI? Error");
            return "";
        }

        System.out.println(connection.getResponseCode());
        System.out.println(connection.getResponseMessage());

        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String response = br.readLine();
        br.close();
        //Integer id = new Gson().fromJson(response, Integer.class);
        return response;
    }
}
